package com.anphu.simplelistview.sync;

import com.anphu.simplelistview.db.KetQua;

import java.util.Date;
import java.util.List;

/**
 * Created by nguyenivan on 12/10/13.
 */
public class SyncContract {
    public Date lastUpdate;
    public List<KetQua> dirtyList;
}
