package com.anphu.simplelistview.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.util.Log;

import com.anphu.simplelistview.CustomApplication;
import com.anphu.simplelistview.db.DaKham;
import com.anphu.simplelistview.db.DaKhamDao;
import com.anphu.simplelistview.db.KetQua;
import com.anphu.simplelistview.db.KetQuaDao;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.WhereCondition;

/**
 * Created by nguyenivan on 12/10/13.
 */
public class OnsiteSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "OnsiteSyncAdapter";
    private static final WhereCondition DIRTY_DAKHAM = DaKhamDao.Properties.Dirty.eq(true);
    private static final WhereCondition DIRTY_KETQUA = KetQuaDao.Properties.Dirty.eq(true);
    private static final WhereCondition MAX_SERVERID_DAKHAM = new WhereCondition.StringCondition(String.format(
        "%s IN (SELECT MAX(%s) FROM %s)", DaKhamDao.Properties.ServerId.columnName,
        DaKhamDao.Properties.ServerId.columnName, DaKhamDao.TABLENAME
    ));
    private static final WhereCondition MAX_SERVERID_KETQUA = new WhereCondition.StringCondition(String.format(
         "%s IN (SELECT MAX(%s) FROM %s)", KetQuaDao.Properties.ServerId.columnName,
         KetQuaDao.Properties.ServerId.columnName, KetQuaDao.TABLENAME
    ));

    private static final Gson GSON = new Gson();
    private static final int CURSOR_LIMIT = 10;

    private Context mContext;
    /**
     * Network connection timeout, in milliseconds.
     */

    private static final int NET_CONNECT_TIMEOUT_MILLIS = 15000;  // 15 seconds
    /**
     * Network read timeout, in milliseconds.
     */

    private static final int NET_READ_TIMEOUT_MILLIS = 10000;  // 10 seconds
    public OnsiteSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;

    }

    public OnsiteSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;
    }

    private void updateKetQuaList(List<KetQua> ketQuaList){
        CustomApplication application = (CustomApplication) mContext.getApplicationContext();
        for (KetQua daKham: ketQuaList) {
            if (daKham.getId()!=null) {
                application.ketQuaDao.update(daKham);
            } else {
                application.ketQuaDao.insert(daKham);
            }
        }
    }

    private void updateDaKhamList(List<DaKham> daKhamList){
        CustomApplication application = (CustomApplication) mContext.getApplicationContext();
        for (DaKham daKham: daKhamList) {
            if (daKham.getId()!=null) {
                application.daKhamDao.update(daKham);
            } else {
                application.daKhamDao.insert(daKham);
            }
        }
    }

    private void updateServerTimeStampList(List<TimeStampDict> timeStampDictList, String tableName){
        final CustomApplication application = (CustomApplication) mContext.getApplicationContext();
        final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sssZ");
        for (TimeStampDict timeStampDict: timeStampDictList) {
            ContentValues args = new ContentValues();
            args.put(DaKhamDao.Properties.ServerTimeStamp.columnName,
                    parser.format(timeStampDict.serverTimeStamp));
            application.db.update(tableName, args, String.format("%s=?",
                    DaKhamDao.Properties.ServerId.columnName, timeStampDict.serverId),null);
        }
    }


    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        if (mContext==null){
            Log.d(TAG, "Context is null, cannot perform sync");
            return;
        }
        if (mContext.getClass()!=CustomApplication.class) {
            Log.d(TAG, String.format(
                    "Context is type %s. Expected com.anphu.simplelistview.CustomApplication" + mContext.getClass().toString()
            ));
            return;
        }
        if (((CustomApplication)mContext).maGoiKham==null || ((CustomApplication)mContext).maGoiKham.equals("") ) {
            Log.d(TAG, "Applicatoin.goiKham is not set, cannot perform sync");
            return;
        }

        Log.i(TAG, "Beginning network synchronization");
        try {
            DaKhamSync daKhamSync = syncDaKham();
            Log.d(TAG, new Gson().toJson((daKhamSync)));
            updateDaKhamList(daKhamSync.daKhamList);
            updateServerTimeStampList(daKhamSync.serverTimeStampList, DaKhamDao.TABLENAME);
        }
        catch (Exception e){
            Log.e(TAG, "Error while syncing DaKham data.", e);
        }

        try {
            KetQuaSync ketQuaSync = syncKetQua();
            Log.d(TAG, new Gson().toJson((ketQuaSync)));
            updateKetQuaList(ketQuaSync.ketQuaList);
            updateServerTimeStampList(ketQuaSync.serverTimeStampList, KetQuaDao.TABLENAME);
        }
        catch (Exception e){
            Log.e(TAG, "Error while syncing KetQua data.", e);
        }

        Log.i(TAG, "End network synchronization");
    }


    private KetQuaSync syncKetQua(){
        final CustomApplication application = (CustomApplication) mContext.getApplicationContext();
        KetQuaSync ketQuaSync=null;
        final List<KetQua> maxServerIdEntity = application.ketQuaDao.queryBuilder()
                .orderAsc(KetQuaDao.Properties.Id)
                .where(MAX_SERVERID_KETQUA)
                .limit(1)
                .list();
        long maxServerId = 0;
        if (maxServerIdEntity.size()>0) {
            maxServerId = maxServerIdEntity.get(0).getServerId();
        }

        final List<KetQua> dirtyList = application.ketQuaDao.queryBuilder()
                .orderAsc(KetQuaDao.Properties.Id)
                .where(DIRTY_KETQUA)
                .limit(CURSOR_LIMIT)
                .list();

        final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
        final HttpPost httpPost = new HttpPost(CustomApplication.KETQUA_URL);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("deviceId", CustomApplication.DEVICE_ID));
            nameValuePairs.add(new BasicNameValuePair("maxServerId", Long.toString(maxServerId)));
            nameValuePairs.add(new BasicNameValuePair("dirtyRecords", GSON.toJson(dirtyList)));
            nameValuePairs.add(new BasicNameValuePair("goiKham", GSON.toJson(application.goiKham)));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);

            final HttpEntity entity = response.getEntity();
            if (entity!=null) {
                String responseString = EntityUtils.toString(entity);
                ketQuaSync = new Gson().fromJson(
                        responseString, KetQuaSync.class
                );
//                        new TypeToken<ArrayList<DaKham>>(){}.getType()
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or
            // IllegalStateException
            httpPost.abort();
            Log.w(TAG, "Error while download KetQua from " + CustomApplication.DAKHAM_URL);
            Log.e(TAG, "Error detail", e);
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }
        return ketQuaSync;
    }

    private DaKhamSync syncDaKham() {
        CustomApplication application = (CustomApplication) mContext.getApplicationContext();
        DaKhamSync daKhamSync=null;
        final List<DaKham> maxServerIdEntity = application.daKhamDao.queryBuilder()
                .orderAsc(DaKhamDao.Properties.Id)
                .where(MAX_SERVERID_DAKHAM)
                .limit(1)
                .list();
        long maxServerId = 0;
        if (maxServerIdEntity.size()>0) {
            maxServerId = maxServerIdEntity.get(0).getServerId();
        }

        final List<DaKham> dirtyList = application.daKhamDao.queryBuilder()
                .orderAsc(DaKhamDao.Properties.Id)
                .where(DIRTY_DAKHAM)
                .limit(CURSOR_LIMIT)
                .list();

        final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
        final HttpPost httpPost = new HttpPost(CustomApplication.DAKHAM_URL);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("deviceId", CustomApplication.DEVICE_ID));
            nameValuePairs.add(new BasicNameValuePair("maxServerId", Long.toString(maxServerId)));
            nameValuePairs.add(new BasicNameValuePair("dirtyRecords", GSON.toJson(dirtyList)));
            nameValuePairs.add(new BasicNameValuePair("goiKham", GSON.toJson(application.goiKham)));


            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);

            final HttpEntity entity = response.getEntity();
            if (entity!=null) {
                String responseString = EntityUtils.toString(entity);
                daKhamSync = new Gson().fromJson(
                        responseString, DaKhamSync.class
                );
//                        new TypeToken<ArrayList<DaKham>>(){}.getType()
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or
            // IllegalStateException
            httpPost.abort();
            Log.w(TAG, "Error while DaKham from " + CustomApplication.DAKHAM_URL);
            Log.e(TAG, "Error detail", e);
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }
        return daKhamSync;
    }

    private static class KetQuaSync {
        public List<KetQua> ketQuaList;
        public List<TimeStampDict> serverTimeStampList;
    }

    private static class DaKhamSync {
        public List<DaKham> daKhamList;
        public List<TimeStampDict> serverTimeStampList;
    }

    private static class TimeStampDict {
        public String serverId;
        public Date serverTimeStamp;
    }

}
