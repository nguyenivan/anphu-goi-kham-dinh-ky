package com.anphu.simplelistview.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

/**
 * Created by nguyenivan on 12/10/13.
 */
public class OnsiteSyncService  extends Service {
    private static final String TAG = "OnsiteSyncService";

    private static final Object sSyncAdapterLock = new Object();
    private static OnsiteSyncAdapter sSyncAdapter = null;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Service created");
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new OnsiteSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    /**
     * Logging-only destructor.
     */
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Service destroyed");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
