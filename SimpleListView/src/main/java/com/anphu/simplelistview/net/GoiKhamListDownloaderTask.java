package com.anphu.simplelistview.net;

import android.app.AlertDialog;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.anphu.simplelistview.CustomApplication;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.anphu.simplelistview.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Created by nguyenivan on 12/11/13.
 */
public class GoiKhamListDownloaderTask extends AsyncTask <Void, Void, ArrayList<GoiKhamItem>> {
    private static final String TAG = "OnsiteGoiKhamListDownloader";

    private final WeakReference<GoiKhamDialog> goiKhamDialogWeakReference;

    public GoiKhamListDownloaderTask(GoiKhamDialog alertDialog) {
        this.goiKhamDialogWeakReference = new WeakReference<GoiKhamDialog>(alertDialog);
    }

    @Override
     protected ArrayList<GoiKhamItem> doInBackground(Void... voids) {
        Log.d(TAG, "Initialize background task.");
        return downloadGoiKhamList();
    }

    @Override
    protected void onPostExecute(ArrayList<GoiKhamItem> goiKhamItemList) {
        if (isCancelled()) {
            goiKhamItemList = null;
        }

        if (goiKhamDialogWeakReference != null) {
            ListView listView = null;
            View infoLayout = null;
            View downloadLayout = null;
            GoiKhamDialog alertDialog = null;
            try {
                alertDialog = goiKhamDialogWeakReference.get();
                listView = (ListView)alertDialog.getDialog().findViewById(R.id.list_goikham);
                infoLayout = alertDialog.getDialog().findViewById(R.id.layout_info);
                downloadLayout =  alertDialog.getDialog().findViewById(R.id.layout_download);
            }
            catch (NullPointerException e) {
                Log.d(TAG, "Cannot find view goikham_list and/or layout_info");
             }
            if (listView != null && infoLayout != null) {
                if (goiKhamItemList != null) {
                    listView.setAdapter(new GoiKhamAdapter(listView.getContext(), goiKhamItemList));
                    downloadLayout.setVisibility(View.VISIBLE);
                    infoLayout.setVisibility(View.GONE);
                    alertDialog.cancelButton();
                } else {
                    // Enable the info layout
                    downloadLayout.setVisibility(View.GONE);
                    infoLayout.setVisibility(View.VISIBLE);
                    alertDialog.okButton();
                }

            }

        }
        Log.d(TAG, "Done.");
    }

    private ArrayList<GoiKhamItem> downloadGoiKhamList() {

        final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
        final HttpPost httpPost = new HttpPost(CustomApplication.GOIKHAM_URL);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("action", "get"));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);

            final HttpEntity entity = response.getEntity();
            if (entity!=null) {
                String responseString = EntityUtils.toString(entity);
                ArrayList<GoiKhamItem> goiKhamItemList = new Gson().fromJson(
                        responseString,
                        new TypeToken<ArrayList<GoiKhamItem>>(){}.getType()
                );
                Log.d(TAG, goiKhamItemList.toString());
                return goiKhamItemList;
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or
            // IllegalStateException
            httpPost.abort();
            Log.w(TAG, "Error while GoiKham from " + CustomApplication.GOIKHAM_URL);
            Log.e(TAG, "Error detail", e);
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }
        return null;
    }
}
