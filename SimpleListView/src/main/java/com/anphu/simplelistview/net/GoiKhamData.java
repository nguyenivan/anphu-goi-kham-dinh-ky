package com.anphu.simplelistview.net;

import com.anphu.simplelistview.db.BenhNhan;
import com.anphu.simplelistview.db.KetQuaMau;

import java.util.ArrayList;

/**
 * Created by nguyenivan on 12/12/13.
 */
public class GoiKhamData {
    public ArrayList<BenhNhan> benhNhanList;
    public ArrayList<KetQuaMau> ketQuaMauList;
}
