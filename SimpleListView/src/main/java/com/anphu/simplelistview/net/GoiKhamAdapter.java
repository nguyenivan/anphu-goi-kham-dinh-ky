package com.anphu.simplelistview.net;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.anphu.simplelistview.R;

import java.util.ArrayList;

/**
 * Created by nguyenivan on 12/10/13.
 */
public class GoiKhamAdapter extends BaseAdapter{
    private ArrayList<GoiKhamItem> mListItems;
    private LayoutInflater mLayoutInflater;

    public GoiKhamAdapter(Context context, ArrayList<GoiKhamItem> goiKhamItemList){

        mListItems = goiKhamItemList;

        //get the layout inflater
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        //getCount() represents how many items are in the list
        return mListItems.size();
    }

    @Override
    //get the data of an item from a specific position
    //i represents the position of the item in the list
    public Object getItem(int i) {
        return mListItems.get(i);
    }

    @Override
    //get the position id of the item from the list
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        // create a ViewHolder reference
        ViewHolder holder;

        //check to see if the reused view is null or not, if is not null then reuse it
        if (view == null) {
            holder = new ViewHolder();

            view = mLayoutInflater.inflate(R.layout.item_simple, null);
            holder.itemName = (TextView) view.findViewById(R.id.text_summary);

            // the setTag is used to store the data within this view
            view.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)view.getTag();
        }

        //get the string item from the position "position" from array list to put it on the TextView
        GoiKhamItem item = mListItems.get(position);
        if (item != null) {
            if (holder.itemName != null) {
                //set the item name on the TextView
                holder.itemName.setText(String.format("%s (%s) ", item.tenGoiKham, item.maGoiKham));
            }
        }

        //this method must return the view corresponding to the data at the specified position.
        return view;

    }

    /**
     * Static class used to avoid the calling of "findViewById" every time the getView() method is called,
     * because this can impact to your application performance when your list is too big. The class is static so it
     * cache all the things inside once it's created.
     */
    private static class ViewHolder {

        protected TextView itemName;

    }
}
