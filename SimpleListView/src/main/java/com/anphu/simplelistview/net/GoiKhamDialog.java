package com.anphu.simplelistview.net;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.anphu.simplelistview.R;

/**
 * Created by nguyenivan on 12/10/13.
 */
public class GoiKhamDialog extends DialogFragment {
    private GoiKhamDataDownloaderTask mAsyncTask;
    private AlertDialog mAlertDialog;
    private View mView;

    public GoiKhamDialog() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mView = getActivity().getLayoutInflater().inflate(R.layout.dialog_goikham, null);
        mAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Download Gói Khám")
                .setView(mView)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (mAsyncTask != null) {
                            mAsyncTask.cancel(true);
                        }
                        dialog.dismiss();
                    }
                })
                .create();

        final ListView listView = (ListView) mView.findViewById(R.id.list_goikham);
        final GoiKhamDialog dialog = this;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View itemView, int i, long l) {
                //Load goi kham to DB
                GoiKhamItem goiKhamItem = (GoiKhamItem) listView.getAdapter().getItem(i);
                mAsyncTask = new GoiKhamDataDownloaderTask(dialog);
                mAsyncTask.execute(goiKhamItem.maGoiKham);
                mView.findViewById(R.id.list_goikham).setVisibility(View.GONE);
                mView.findViewById(R.id.progress_download).setVisibility(View.VISIBLE);

                SharedPreferences.Editor editor= PreferenceManager.getDefaultSharedPreferences(
                        getActivity()).edit();
                editor.putString(
                        getResources().getString(R.string.key_goi_kham),
                        goiKhamItem.tenGoiKham
                );


                Toast.makeText(getActivity(), String.format("Downloading %s(%s)",
                        goiKhamItem.tenGoiKham, goiKhamItem.maGoiKham), Toast.LENGTH_SHORT).show();
            }
        });

        new GoiKhamListDownloaderTask(this).execute();
        return mAlertDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void okButton() {
        Button button = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        if (button!=null) {
            button.setText("Ok");
        }
    }
    public void cancelButton() {
        Button button = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        if (button!=null) {
            button.setText("Cancel");
        }
    }
}
