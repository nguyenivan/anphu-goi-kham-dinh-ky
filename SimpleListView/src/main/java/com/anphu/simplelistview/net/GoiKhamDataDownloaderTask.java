package com.anphu.simplelistview.net;


import android.database.sqlite.SQLiteDatabase;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.anphu.simplelistview.CustomApplication;
import com.anphu.simplelistview.R;
import com.anphu.simplelistview.db.BenhNhan;
import com.anphu.simplelistview.db.BenhNhanDao;
import com.anphu.simplelistview.db.KetQuaMau;
import com.anphu.simplelistview.db.KetQuaMauDao;
import com.anphu.simplelistview.db.StringUtils;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguyenivan on 12/12/13.
 */
public class GoiKhamDataDownloaderTask extends AsyncTask<Integer, Void, GoiKhamData> {

    private static final String TAG = "OnsiteGoiKhamDataDownloaderTask";
    private final WeakReference<GoiKhamDialog> goiKhamDialogWeakReference;

    public GoiKhamDataDownloaderTask(GoiKhamDialog dialog) {
        goiKhamDialogWeakReference = new WeakReference<GoiKhamDialog>(dialog);
    }


    @Override
    protected GoiKhamData doInBackground(Integer... integers) {
        return downloadGoiKhamData(integers[0]);
    }

    @Override
    protected void onPostExecute(GoiKhamData goiKhamData) {
        if (isCancelled()) {
            goiKhamData = null;
        }

        GoiKhamDialog alertDialog = null;

        if (goiKhamDialogWeakReference != null) {
            alertDialog = goiKhamDialogWeakReference.get();
        }

        if (alertDialog != null && goiKhamData!=null) {
            try {
                // Clear the table BenhNhan and KetQuaMau
                CustomApplication customApplication = (CustomApplication)alertDialog.getActivity().getApplication();
                SQLiteDatabase db = customApplication.db;

                BenhNhanDao.dropTable(db, true);
                KetQuaMauDao.dropTable(db, true);
                BenhNhanDao.createTable(db, true);
                KetQuaMauDao.createTable(db, true);
                for (BenhNhan benhNhan : goiKhamData.benhNhanList) {
                    benhNhan.setHoTenNormal(
                            StringUtils.removeAccent(benhNhan.getHoTen())
                    );

                    customApplication.benhNhanDao.insert(benhNhan);
                }
                for (KetQuaMau ketQuaMau : goiKhamData.ketQuaMauList) {
                    ketQuaMau.setChanDoanNormal(
                            StringUtils.removeAccent(ketQuaMau.getChanDoan())
                    );
                    customApplication.ketQuaMauDao.insert(ketQuaMau);
                }

            }
            catch (Exception e) {
                Log.e(TAG, "Error", e);
            }
            try {
                alertDialog.getDialog().findViewById(R.id.layout_info).setVisibility(View.VISIBLE);
                alertDialog.getDialog().findViewById(R.id.layout_download).setVisibility(View.GONE);
                alertDialog.okButton();
                ((TextView)alertDialog.getDialog().findViewById(R.id.text_info)).setText(R.string.download_complete);
            }
            catch (ClassCastException e) {
                Log.e(TAG, "Error", e);
            }
        }
    }

    private GoiKhamData downloadGoiKhamData(Integer pk) {
        final AndroidHttpClient httpClient = AndroidHttpClient.newInstance("Android");
        final HttpPost httpPost = new HttpPost(String.format("%s%s/", CustomApplication.GOIKHAM_URL, pk));
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("action", "get"));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpClient.execute(httpPost);

            final HttpEntity entity = response.getEntity();
            if (entity!=null) {
                String responseString = EntityUtils.toString(entity);
                GoiKhamData goiKhamData = new Gson().fromJson(
                        responseString,
                        GoiKhamData.class
                );
                Log.d(TAG, goiKhamData.toString());
                return goiKhamData;
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or
            // IllegalStateException
            httpPost.abort();
            Log.w(TAG, "Error while GoiKham from " + CustomApplication.GOIKHAM_URL);
            Log.e(TAG, "Error detail", e);
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
        }
        return null;
    }
}
