package com.anphu.simplelistview;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;


/**
 * Created by nguyenivan on 11/30/13.
 */
public class ModalityCollection {
    private static ModalityCollection instance;
    private HashMap<String, Modality> mModalityMap;
    private Collection<String> mSelectedCode;



    public ModalityCollection(Context context) {
        Resources res = context.getResources();
        String[] stringArray = res.getStringArray(R.array.modality_array);
        mModalityMap = new HashMap<String, Modality>();
        for (String entry : stringArray){
            String[] splitResult = entry.split("\\|",2);
            mModalityMap.put(splitResult[0], new Modality(splitResult[1], false));
        }
        mSelectedCode = new ArrayList<String>();
    }

    public static Modality getModality(String modalityCode){
        return instance.mModalityMap.get(modalityCode);
    }

    public static Set<String> getModalityCodeList(){
        return instance.mModalityMap.keySet();
    }

    public static void selectModality(String modalityCode){
        Modality modality = instance.mModalityMap.get(modalityCode);
        modality.isSelected = true;
        if (!instance.mSelectedCode.contains(modalityCode)){
            instance.mSelectedCode.add(modalityCode);
        }
    }

    public static void deselectModality(String modalityCode){
        Modality modality = instance.mModalityMap.get(modalityCode);
        modality.isSelected = false;
        if (instance.mSelectedCode.contains(modalityCode)){
            instance.mSelectedCode.remove(modalityCode);
        }
    }

    public boolean isSelected(String modalityCode) {
        Modality modality = mModalityMap.get(modalityCode);
        return modality.isSelected;
    }

    public static String[] getSelectedCodes(){
        return instance.mSelectedCode.toArray(new String[instance.mSelectedCode.size()]);
    }

    public static ModalityCollection initInstance(Context context){
        if (instance==null){
            instance = new ModalityCollection(context);
        }
        return instance;
    }

    public static String getConcatenatedTitle() {
        ArrayList<String> selectedModality = new ArrayList<String>();
        for (String key: instance.mModalityMap.keySet()){
            if (instance.mModalityMap.get(key).isSelected) {
                selectedModality.add(instance.mModalityMap.get(key).modalityLabel);
            }
        }
        return TextUtils.join(" + ", selectedModality);
    }
}