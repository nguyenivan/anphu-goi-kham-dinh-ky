package com.anphu.simplelistview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.anphu.simplelistview.db.DaoMaster;
import com.anphu.simplelistview.net.GoiKhamDialog;
import com.anphu.simplelistview.sync.SyncUtils;
//import android.app.FragmentManager;

public class MainActivity extends ActionBarActivity {
    private static final String TAG = "com.anphu.simplelistview.MainActivity";
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SyncUtils.CreateSyncAccount(MainActivity.this);
        setContentView(R.layout.activity_main);
        Object [] keyArray = ModalityCollection.getModalityCodeList().toArray();
        for (int counter=0; counter< keyArray.length / 3; counter ++) {
            String [] tmpArray = new String [] {null, null, null};
            for (int i=0; i<3; i++){
                if (counter*3+i<keyArray.length){
                    tmpArray[i]=(String)keyArray[counter*3+i];
                }
            }
            ModalityFragment fragment = new ModalityFragment(tmpArray[0], tmpArray[1], tmpArray[2] );
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, UserPreferencesActivity.class));
                return true;
            case R.id.action_edit:
                startActivity(new Intent(MainActivity.this, PatientActivity.class));
                return true;
            case R.id.action_resetdb:
                new AlertDialog.Builder(this)
                        .setTitle("Cảnh Báo")
                        .setMessage("Toàn bộ dữ liệu sẽ bị xoá!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                DaoMaster.dropAllTables(((CustomApplication)getApplication()).db, true);
                                DaoMaster.createAllTables(((CustomApplication)getApplication()).db, true);
                                Toast.makeText(MainActivity.this, "Đã xoá toàn bộ dữ liệu", Toast.LENGTH_SHORT).show();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();

                return true;
            case R.id.action_goikham:
                showEditDialog();
                return true;
            case R.id.action_sync:
                SyncUtils.TriggerRefresh();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        GoiKhamDialog dialog = new GoiKhamDialog();
        dialog.show(fm, "fragment_goikham");
    }

}
