package com.anphu.simplelistview;

/**
 * Created by nguyenivan on 11/30/13.
 */
public class Modality {

    public String modalityLabel;
    public Boolean isSelected;

    public Modality(String modalityLabel, Boolean isSelected) {
        this.modalityLabel = modalityLabel;
        this.isSelected = isSelected;
    }
}
