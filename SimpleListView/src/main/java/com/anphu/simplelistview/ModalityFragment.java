package com.anphu.simplelistview;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by nguyenivan on 11/25/13.
 */
public class ModalityFragment extends Fragment {

    public String button01, button02, button03;

    public ModalityFragment(String button01, String button02, String button03){
        this.button01 = button01;
        this.button02 = button02;
        this.button03 = button03;
    }

    public ModalityFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_modality,
                container, false);
        setButton((ModalityToggleButton)(view.findViewById(R.id.button01)), button01);
        setButton((ModalityToggleButton)(view.findViewById(R.id.button02)), button02);
        setButton((ModalityToggleButton)(view.findViewById(R.id.button03)), button03);
        return view;
    }

    private void setButton(ModalityToggleButton button, String modalityId){
        button.modalityId = modalityId;
        if (modalityId!=null){
            button.setTextOn(ModalityCollection.getModality(modalityId).modalityLabel + "\n(OK)");
            button.setTextOff(ModalityCollection.getModality(modalityId).modalityLabel);
            button.setText(ModalityCollection.getModality(modalityId).modalityLabel);
        }
        final ModalityToggleButton b = button;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (b.modalityId != null) {
                    if (b.isChecked()) {
                        ModalityCollection.selectModality(b.modalityId);
                    } else{
                        ModalityCollection.deselectModality(b.modalityId);
                    }
                    String title = ModalityCollection.getConcatenatedTitle();
                    if (!title.equals("")) {
                        getActivity().setTitle(title);
                    } else {
                        getActivity().setTitle(getResources().getString(R.string.app_name));
                    }
                }
            }
        });
    }

}

