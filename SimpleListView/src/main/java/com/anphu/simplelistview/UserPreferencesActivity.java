package com.anphu.simplelistview;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;

public class UserPreferencesActivity extends PreferenceActivity {
    private static final String TAG = "UserPreferencesActivity";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Open preferences");
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new UserPreferencesFragment()).commit();
    }

    public static class UserPreferencesFragment extends PreferenceFragment {
        private final static String TAG = "UserPreferencesFragment";

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Log.i(TAG, "Create preferences fragment");
            addPreferencesFromResource(R.xml.preferences);
        }
    }
}