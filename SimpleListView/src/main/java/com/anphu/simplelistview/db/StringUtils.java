package com.anphu.simplelistview.db;

/**
 * Created by nguyenivan on 12/14/13.
 */
import java.text.Normalizer;
import java.util.Date;
import java.util.regex.Pattern;


public class StringUtils{
    public static String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replace("đ", "");
    }
//    public static String normalize(String f)
//    {
//        f = f.replace('\340', 'a');
//        f = f.replace('\341', 'a');
//        f = f.replace('\u1EA3', 'a');
//        f = f.replace('\343', 'a');
//        f = f.replace('\u1EA1', 'a');
//        f = f.replace('\u0103', 'a');
//        f = f.replace('\u1EB1', 'a');
//        f = f.replace('\u1EAF', 'a');
//        f = f.replace('\u1EB3', 'a');
//        f = f.replace('\u1EB5', 'a');
//        f = f.replace('\u1EB7', 'a');
//        f = f.replace('\342', 'a');
//        f = f.replace('\u1EA7', 'a');
//        f = f.replace('\u1EA5', 'a');
//        f = f.replace('\u1EA9', 'a');
//        f = f.replace('\u1EAB', 'a');
//        f = f.replace('\u1EAD', 'a');
//        f = f.replace('\300', 'a');
//        f = f.replace('\301', 'a');
//        f = f.replace('\u1EA2', 'a');
//        f = f.replace('\303', 'a');
//        f = f.replace('\u1EA0', 'a');
//        f = f.replace('\u0102', 'a');
//        f = f.replace('\u1EB0', 'a');
//        f = f.replace('\u1EAE', 'a');
//        f = f.replace('\u1EB2', 'a');
//        f = f.replace('\u1EB4', 'a');
//        f = f.replace('\u1EB6', 'a');
//        f = f.replace('\302', 'a');
//        f = f.replace('\u1EA6', 'a');
//        f = f.replace('\u1EA4', 'a');
//        f = f.replace('\u1EA8', 'a');
//        f = f.replace('\u1EAA', 'a');
//        f = f.replace('\u1EAC', 'a');
//        f = f.replace('\u0111', 'd');
//        f = f.replace('\u0110', 'd');
//        f = f.replace('\350', 'e');
//        f = f.replace('\351', 'e');
//        f = f.replace('\u1EBB', 'e');
//        f = f.replace('\u1EBD', 'e');
//        f = f.replace('\u1EB9', 'e');
//        f = f.replace('\352', 'e');
//        f = f.replace('\u1EC1', 'e');
//        f = f.replace('\u1EBF', 'e');
//        f = f.replace('\u1EC3', 'e');
//        f = f.replace('\u1EC5', 'e');
//        f = f.replace('\u1EC7', 'e');
//        f = f.replace('\310', 'e');
//        f = f.replace('\311', 'e');
//        f = f.replace('\u1EBA', 'e');
//        f = f.replace('\u1EBC', 'e');
//        f = f.replace('\u1EB8', 'e');
//        f = f.replace('\312', 'e');
//        f = f.replace('\u1EC0', 'e');
//        f = f.replace('\u1EBE', 'e');
//        f = f.replace('\u1EC2', 'e');
//        f = f.replace('\u1EC4', 'e');
//        f = f.replace('\u1EC6', 'e');
//        f = f.replace('\354', 'i');
//        f = f.replace('\355', 'i');
//        f = f.replace('\u1EC9', 'i');
//        f = f.replace('\u0129', 'i');
//        f = f.replace('\u1ECB', 'i');
//        f = f.replace('\314', 'i');
//        f = f.replace('\315', 'i');
//        f = f.replace('\u1EC8', 'i');
//        f = f.replace('\u0128', 'i');
//        f = f.replace('\u1ECA', 'i');
//        f = f.replace('\362', 'o');
//        f = f.replace('\363', 'o');
//        f = f.replace('\u1ECF', 'o');
//        f = f.replace('\365', 'o');
//        f = f.replace('\u1ECD', 'o');
//        f = f.replace('\364', 'o');
//        f = f.replace('\u1ED3', 'o');
//        f = f.replace('\u1ED1', 'o');
//        f = f.replace('\u1ED5', 'o');
//        f = f.replace('\u1ED7', 'o');
//        f = f.replace('\u1ED9', 'o');
//        f = f.replace('\u01A1', 'o');
//        f = f.replace('\u1EDD', 'o');
//        f = f.replace('\u1EDB', 'o');
//        f = f.replace('\u1EDF', 'o');
//        f = f.replace('\u1EE1', 'o');
//        f = f.replace('\u1EE3', 'o');
//        f = f.replace('\322', 'o');
//        f = f.replace('\323', 'o');
//        f = f.replace('\u1ECE', 'o');
//        f = f.replace('\325', 'o');
//        f = f.replace('\u1ECC', 'o');
//        f = f.replace('\324', 'o');
//        f = f.replace('\u1ED2', 'o');
//        f = f.replace('\u1ED0', 'o');
//        f = f.replace('\u1ED4', 'o');
//        f = f.replace('\u1ED6', 'o');
//        f = f.replace('\u1ED8', 'o');
//        f = f.replace('\u01A0', 'o');
//        f = f.replace('\u1EDC', 'o');
//        f = f.replace('\u1EDA', 'o');
//        f = f.replace('\u1EDE', 'o');
//        f = f.replace('\u1EE0', 'o');
//        f = f.replace('\u1EE2', 'o');
//        f = f.replace('\371', 'u');
//        f = f.replace('\372', 'u');
//        f = f.replace('\u1EE7', 'u');
//        f = f.replace('\u0169', 'u');
//        f = f.replace('\u1EE5', 'u');
//        f = f.replace('\u01B0', 'u');
//        f = f.replace('\u1EEB', 'u');
//        f = f.replace('\u1EE9', 'u');
//        f = f.replace('\u1EED', 'u');
//        f = f.replace('\u1EEF', 'u');
//        f = f.replace('\u1EF1', 'u');
//        f = f.replace('\331', 'u');
//        f = f.replace('\332', 'u');
//        f = f.replace('\u1EE6', 'u');
//        f = f.replace('\u0168', 'u');
//        f = f.replace('\u1EE4', 'u');
//        f = f.replace('\u01AF', 'u');
//        f = f.replace('\u1EEA', 'u');
//        f = f.replace('\u1EE8', 'u');
//        f = f.replace('\u1EEC', 'u');
//        f = f.replace('\u1EEE', 'u');
//        f = f.replace('\u1EF0', 'u');
//        f = f.replace('\u1EF3', 'y');
//        f = f.replace('\375', 'y');
//        f = f.replace('\u1EF7', 'y');
//        f = f.replace('\u1EF9', 'y');
//        f = f.replace('\u1EF5', 'y');
//        f = f.replace('Y', 'y');
//        f = f.replace('\u1EF2', 'y');
//        f = f.replace('\335', 'y');
//        f = f.replace('\u1EF6', 'y');
//        f = f.replace('\u1EF8', 'y');
//        f = f.replace('\u1EF4', 'y');
//        return f;
//    }

    public static Object nullToEmpty(Object o) {
        if (o==null) {
            return "";
        }
        else {
            if (o.getClass()==Date.class) {
                return String.format("%tY", o);
            }
            else {
                return o;
            }
        }
    }

    public static Object nullToEmpty(Object o, Object defaultValue) {
        if (o==null) {
            return defaultValue;
        }
        else {
            return o;
        }
    }
}