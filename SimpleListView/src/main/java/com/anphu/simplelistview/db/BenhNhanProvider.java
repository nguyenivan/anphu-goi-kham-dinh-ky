package com.anphu.simplelistview.db;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import de.greenrobot.dao.DaoLog;

import com.anphu.simplelistview.CustomApplication;

import java.util.HashMap;

/* Copy this code snippet into your AndroidManifest.xml inside the
<application> element:

    <provider
            android:name="${contentProvider.javaPackage}.${contentProvider.className}"
            android:authorities="${contentProvider.authority}"/>
    */

public class BenhNhanProvider extends ContentProvider {

    public static final String AUTHORITY = "com.anphu.simplelistview.db.BenhNhanProvider";
    public static final String BASE_PATH = "benhnhan";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/" + BASE_PATH;
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/" + BASE_PATH;

    private static final String TABLENAME = BenhNhanDao.TABLENAME;
    private static final String PK = BenhNhanDao.Properties.Id.columnName;

    private static final int BENHNHAN_DIR = 0;
    private static final int BENHNHAN_ID = 1;
    private static final int SEARCH_SUGGEST = 2;

    private static final UriMatcher sURIMatcher;

    static {
        sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, BENHNHAN_DIR);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", BENHNHAN_ID);
        sURIMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST);
        sURIMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH_SUGGEST);
    }

    private static final HashMap<String, String> SEARCH_SUGGEST_PROJECTION_MAP;

    static {
        SEARCH_SUGGEST_PROJECTION_MAP = new HashMap<String, String>();
        SEARCH_SUGGEST_PROJECTION_MAP.put(BenhNhanDao.Properties.Id.columnName, BenhNhanDao.Properties.Id.columnName);
        SEARCH_SUGGEST_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_TEXT_1,
                BenhNhanDao.Properties.HoTen.columnName + " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1);
        SEARCH_SUGGEST_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_TEXT_2,
                BenhNhanDao.Properties.GioiTinh.columnName + " AS " + SearchManager.SUGGEST_COLUMN_TEXT_2);
        SEARCH_SUGGEST_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
                BenhNhanDao.Properties.Id.columnName + " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
    }

    /**
     * This must be set from outside, it's recommended to do this inside your Application object.
     * Subject to change (static isn't nice).
     */
    private DaoSession daoSession;

    @Override
    public boolean onCreate() {
        daoSession = ((CustomApplication) getContext().getApplicationContext()).daoSession;
        if (daoSession == null) {
            throw new IllegalStateException("DaoSession must be set before content provider is created");
        }
        DaoLog.d("Content Provider started: " + CONTENT_URI);
        return true;
    }

    protected SQLiteDatabase getDatabase() {
        if (daoSession == null) {
            throw new IllegalStateException("DaoSession must be set during content provider is active");
        }
        return daoSession.getDatabase();
    }

//        ##########################################
//        ########## Insert ##############
//        ##########################################
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        long id = 0;
        String path = "";
        switch (uriType) {
            case BENHNHAN_DIR:
                id = getDatabase().insert(TABLENAME, null, values);
                path = BASE_PATH + "/" + id;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(path);
    }

//        ##########################################
//        ########## Delete ##############
//        ##########################################
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase db = getDatabase();
        int rowsDeleted = 0;
        String id;
        switch (uriType) {
            case BENHNHAN_DIR:
                rowsDeleted = db.delete(TABLENAME, selection, selectionArgs);
                break;
            case BENHNHAN_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = db.delete(TABLENAME, PK + "=" + id, null);
                } else {
                    rowsDeleted = db.delete(TABLENAME, PK + "=" + id + " and "
                            + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

//        ##########################################
//        ########## Update ##############
//        ##########################################
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase db = getDatabase();
        int rowsUpdated = 0;
        String id;
        switch (uriType) {
            case BENHNHAN_DIR:
                rowsUpdated = db.update(TABLENAME, values, selection, selectionArgs);
                break;
            case BENHNHAN_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = db.update(TABLENAME, values, PK + "=" + id, null);
                } else {
                    rowsUpdated = db.update(TABLENAME, values, PK + "=" + id
                            + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

//        ##########################################
//        ########## Query ##############
//        ##########################################
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLENAME);
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case SEARCH_SUGGEST:
                selectionArgs = new String[]{"%" + selectionArgs[0] + "%", "%" + selectionArgs[0] + "%"};
                queryBuilder.setProjectionMap(SEARCH_SUGGEST_PROJECTION_MAP);
                break;
            case BENHNHAN_DIR:
                break;
            case BENHNHAN_ID:
                queryBuilder.appendWhere(PK + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = getDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

//        ##########################################
//        ########## GetType ##############
//        ##########################################
    @Override
    public final String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case BENHNHAN_DIR:
                return CONTENT_TYPE;
            case BENHNHAN_ID:
                return CONTENT_ITEM_TYPE;
            case SEARCH_SUGGEST:
                return null;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

}
