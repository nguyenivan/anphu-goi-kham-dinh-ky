package com.anphu.simplelistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.anphu.simplelistview.db.KetQuaMau;
import com.anphu.simplelistview.db.KetQuaMauDao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition;

/**
 * Created by nguyenivan on 12/16/13.
 */
public class DiagnosticListAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private List<KetQuaMau> mCursor;
    private KetQuaMauDao mKetQuaMauDao;
    private String mOrderBy;
    private String[] mModalityList;
    private static final Integer CURSOR_LIMIT = 200;
    private static final Integer MIN_QUERY_LENGTH = 2;
    private WhereCondition mModalityCondition;

    public DiagnosticListAdapter(Context context){
        String phanLoaiColumn = KetQuaMauDao.Properties.PhanLoai.columnName;
        String mucKhamColumn = KetQuaMauDao.Properties.MucKham.columnName;
        CustomApplication customApplication =  (CustomApplication)context.getApplicationContext();
        mOrderBy = String.format("%s, %s %s", mucKhamColumn, phanLoaiColumn, "COLLATE NOCASE ASC");
        mKetQuaMauDao = customApplication.ketQuaMauDao;
        mModalityList = ModalityCollection.getSelectedCodes();
        mModalityCondition =  KetQuaMauDao.Properties.MucKham.in(mModalityList);
        mCursor = mKetQuaMauDao.queryBuilder().where(mModalityCondition).orderRaw(mOrderBy).limit(CURSOR_LIMIT).list();
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mCursor.size();
    }

    @Override
    public Object getItem(int i) {
        return mCursor.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mCursor.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_diagnostic, null);
        }
        KetQuaMau ketQuaMau = mCursor.get(i);
        TextView phanLoai = (TextView) convertView.findViewById(R.id.text_preamble);
        phanLoai.setText(ketQuaMau.getPhanLoai());
        TextView chanDoan = (TextView) convertView.findViewById(R.id.text_summary);
        chanDoan.setText(ketQuaMau.getChanDoan());
        return convertView;
    }


    public void filter(Collection<WhereCondition> conditions){
        QueryBuilder<KetQuaMau> queryBuilder = mKetQuaMauDao.queryBuilder().orderRaw(mOrderBy).limit(CURSOR_LIMIT);
        if (conditions.size()>0) {
            WhereCondition where = mModalityCondition;
            for (WhereCondition cond: conditions) {
                    where = queryBuilder.or(where, cond);
            }
            mCursor = queryBuilder.where(where).list();
        } else {
            mCursor = queryBuilder.where(mModalityCondition).orderRaw(mOrderBy).limit(CURSOR_LIMIT).list();
        }
        notifyDataSetChanged();
    }

    public void filter(String query) {
        QueryBuilder<KetQuaMau> queryBuilder = mKetQuaMauDao.queryBuilder();
        String[] tokens = query.split("\\s+");
        for (String t : tokens) {
            WhereCondition.StringCondition condition = new WhereCondition.StringCondition(
                    String.format("(' ' || %s || ' ') LIKE ?", KetQuaMauDao.Properties.ChanDoanNormal.columnName),
                    String.format("%%%s%%", t)
            );
            queryBuilder.where(condition);
        }
        mCursor = queryBuilder.where(mModalityCondition).orderRaw(mOrderBy).limit(CURSOR_LIMIT).list();
        notifyDataSetChanged();
    }
}