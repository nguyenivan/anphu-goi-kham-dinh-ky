package com.anphu.simplelistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.anphu.simplelistview.db.DaKham;
import com.anphu.simplelistview.db.DaKhamDao;
import com.anphu.simplelistview.db.KetQua;
import com.anphu.simplelistview.db.KetQuaMau;

import java.util.Collection;
import java.util.HashMap;

/**
 * Created by nguyenivan on 12/16/13.
 */
public class DiagnosticActivity extends ActionBarActivity {

    private HashMap<String,KetQua> mKetQuaCache;
    private String[] mModalityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mKetQuaCache = new HashMap<String, KetQua>();
        mModalityList = ModalityCollection.getSelectedCodes();

        setContentView(R.layout.activity_diagnostic);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new DiagnosticFragment()).commit();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void addKetQuaMau(KetQuaMau ketQuaMau){
        if (!mKetQuaCache.containsKey(ketQuaMau.getMaSo())){
            KetQua ketQua = ketQuaFactory(ketQuaMau);
            mKetQuaCache.put(ketQuaMau.getMaSo(), ketQua);
            String summary = "Chẩn đoán: ";
            for (KetQua kq: mKetQuaCache.values()) {
                summary += kq.getChanDoan() + ". ";
            }
            ((TextView)findViewById(R.id.summary)).setText(summary);
        }
    }

    private KetQua ketQuaFactory(KetQuaMau ketQuaMau){
        CustomApplication application = (CustomApplication)getApplication();
        KetQua ketQua = new KetQua();
        ketQua.setMaSo (ketQuaMau.getMaSo());
        ketQua.setMaSoBenhNhan(application.benhNhan.getMaSo());
        ketQua.setHoTen(application.benhNhan.getHoTen());
        ketQua.setGoiKham(application.goiKham);
        ketQua.setChanDoan(ketQuaMau.getChanDoan());
        ketQua.setChanDoanNormal(ketQuaMau.getChanDoanNormal());
        ketQua.setMucKham(ketQuaMau.getMucKham());
        ketQua.setPhanLoai(ketQuaMau.getPhanLoai());
        ketQua.setBacSy (application.bacSy);
        ketQua.setHuy(false);
        ketQua.setDirty(true);
        return ketQua;
    }

    public void ketThucKham() {
        CustomApplication application = (CustomApplication)getApplication();
        for (KetQua ketQua: mKetQuaCache.values()) {
            if (ketQua.getId()!=null){
                application.ketQuaDao.update(ketQua);
            } else {
                application.ketQuaDao.insert(ketQua);
            }
        }
        String maSo = application.benhNhan.getMaSo();
        for (String mucKham: mModalityList) {
            long countDaKham = application.daKhamDao.queryBuilder().where(
                    DaKhamDao.Properties.MaSoBenhNhan.eq(maSo),
                    DaKhamDao.Properties.MucKham.eq(mucKham)
            ).limit(1).count();
            if (countDaKham==0) {
                DaKham daKham = new DaKham(null, maSo, application.benhNhan.getHoTen(), mucKham, false, true, null, null);
                application.daKhamDao.insert(daKham);
            }
        }

        mKetQuaCache.clear();

        startActivity(
                new Intent(DiagnosticActivity.this, PatientActivity.class)
        );

    }
}