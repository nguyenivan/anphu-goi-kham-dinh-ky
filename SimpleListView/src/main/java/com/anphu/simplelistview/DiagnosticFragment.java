package com.anphu.simplelistview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.anphu.simplelistview.db.KetQuaDao;
import com.anphu.simplelistview.db.KetQuaMau;
import com.anphu.simplelistview.db.KetQuaMauDao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import de.greenrobot.dao.query.WhereCondition;

/**
 * Created by nguyenivan on 12/16/13.
 */
public class DiagnosticFragment extends Fragment {
    private static final String TAG = "DiagnosticFragment";
    private DiagnosticListAdapter mAdapter;
    private ListView mListView;
    private ArrayList<ToggleButton> mGradingButtons;
    private static HashMap<String, WhereCondition> sConditionMap;

    static {
        sConditionMap=new HashMap<String, WhereCondition>();
        sConditionMap.put("pl1", KetQuaMauDao.Properties.PhanLoai.eq("1"));
        sConditionMap.put("pl2", KetQuaMauDao.Properties.PhanLoai.eq("2"));
        sConditionMap.put("pl3", KetQuaMauDao.Properties.PhanLoai.eq("3"));
        sConditionMap.put("pl4", KetQuaMauDao.Properties.PhanLoai.eq("4"));
        sConditionMap.put("pl56", KetQuaMauDao.Properties.PhanLoai.in("5", "6"));
    }



    public DiagnosticFragment(){
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mAdapter = new DiagnosticListAdapter(getActivity().getApplicationContext());
        mGradingButtons = new ArrayList<ToggleButton>(4);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diagnostic, container, false);
        final DiagnosticActivity activity = ((DiagnosticActivity)getActivity());
        mListView = (ListView) view.findViewById(R.id.list_diagnostic);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                KetQuaMau ketQuaMau = (KetQuaMau)mAdapter.getItem(i);
                activity.addKetQuaMau(ketQuaMau);
                Toast.makeText(getActivity(), "Chẩn đoán: " + (ketQuaMau ).getChanDoan(), Toast.LENGTH_SHORT).show();
            }
        });

        mGradingButtons.add((ToggleButton)view.findViewById(R.id.pl1));
        mGradingButtons.add((ToggleButton)view.findViewById(R.id.pl2));
        mGradingButtons.add((ToggleButton)view.findViewById(R.id.pl3));
        mGradingButtons.add((ToggleButton)view.findViewById(R.id.pl4));
        mGradingButtons.add((ToggleButton)view.findViewById(R.id.pl56));
        for (ToggleButton button: mGradingButtons) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((ToggleButton)view).isChecked()){
                        for (ToggleButton button: mGradingButtons) {
                            if (button.getTag()!=view.getTag()) {
                                button.setChecked(false);
                            }
                        }
                    }
                    filterKetQuaMau();
                }
            });
        }

        final EditText editText=(EditText)view.findViewById(R.id.edit_search);
        editText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
                mAdapter.filter(s.toString());
                for (ToggleButton button: mGradingButtons) {
                    button.setChecked(false);
                }
            }
        });

        final Button emptyButton = (Button)view.findViewById(R.id.button_empty);
        emptyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("");
                editText.requestFocus();
            }
        });

        final Button saveButton = (Button)view.findViewById(R.id.button_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.ketThucKham();
            }
        });

        return view;
    }

    private void filterKetQuaMau() {
        ArrayList<WhereCondition> conds = new ArrayList<WhereCondition>();
        for (ToggleButton button: mGradingButtons) {
            if (button.isChecked()) {
                conds.add(sConditionMap.get(button.getTag()));
            }
        }
        mAdapter.filter(conds);
    }



}

