package com.anphu.simplelistview;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anphu.simplelistview.db.BenhNhan;
import com.anphu.simplelistview.db.BenhNhanDao;
import com.anphu.simplelistview.db.DaKham;
import com.anphu.simplelistview.db.DaKhamDao;
import com.anphu.simplelistview.db.KetQua;
import com.anphu.simplelistview.db.KetQuaDao;
import com.anphu.simplelistview.db.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition;


/**
 * Created by nguyenivan on 12/3/13.
 */

public class PatientListAdapter extends BaseExpandableListAdapter {
    private static final String TAG = "PatientListAdapter";
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<BenhNhan> mCursor;

    private String mOrderBy;
    private BenhNhanDao mBenhNhanDao;
    private DaKhamDao mDaKhamDao;
    private KetQuaDao mKetQuaDao;
    private static final Integer CURSOR_LIMIT = 20;
    private static final Integer MIN_QUERY_LENGTH = 2;
    private static final HashMap<String, String> sGioiTinhMap;
    static {
        sGioiTinhMap = new HashMap<String, String>(2);
        sGioiTinhMap.put("nam", "Nam");
        sGioiTinhMap.put("nu", "Nữ");
    }


    public PatientListAdapter(Context context){
        mContext = context;
        String hoTenColumn = BenhNhanDao.Properties.HoTen.columnName;
        CustomApplication customApplication =  (CustomApplication)context.getApplicationContext();
        mOrderBy = hoTenColumn + " COLLATE NOCASE ASC";
        mBenhNhanDao  = customApplication.benhNhanDao;
        mDaKhamDao  = customApplication.daKhamDao;
        mKetQuaDao = customApplication.ketQuaDao;
        mCursor = mBenhNhanDao.queryBuilder().orderRaw(mOrderBy).limit(CURSOR_LIMIT).list();

        //get the layout inflater
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setQuery(String query) {
        List<BenhNhan> lastCursor = mCursor;
        mCursor = getBenhNhanList(query);
        notifyDataSetChanged();
    }

    private List<KetQua> getKetQuaList(String maSo) {
        WhereCondition condition1 = KetQuaDao.Properties.MaSoBenhNhan.eq(maSo);
        WhereCondition condition2 = KetQuaDao.Properties.Huy.notEq(true);
        List<KetQua> ret = mKetQuaDao.queryBuilder().where(condition1, condition2).list();
        return ret;
    }

    private List<BenhNhan> getBenhNhanList(String partialValue) {
        // TODO Possible memory Leak here. Need Fix.
        // TODO If param is empty, list all waiting patients
        if (partialValue==null || partialValue.trim().equals("") || partialValue.length()<MIN_QUERY_LENGTH) {
            return mCursor;
        }

        Integer maSo = null;

        try {
            maSo = Integer.parseInt(partialValue);
            Log.d(TAG, "Searching for MaSo");
        } catch (NumberFormatException e)
        {
            Log.d(TAG, "Searching for HoTen");
        }

        QueryBuilder<BenhNhan> queryBuilder = mBenhNhanDao.queryBuilder();
        if (maSo != null) {
            // If param is numeric, search for exact match in
            WhereCondition condition = BenhNhanDao.Properties.MaSo.like(maSo.toString());
            queryBuilder.where(condition);
        } else {
            String[] tokens = partialValue.split("\\s+");
            for (String t : tokens) {
                WhereCondition.StringCondition condition = new WhereCondition.StringCondition(
                    String.format("(' ' || %s || ' ') LIKE ?", BenhNhanDao.Properties.HoTenNormal.columnName),
                    String.format("%%%s%%", t)
                );
                queryBuilder.where(condition);
            }
        }
        List<BenhNhan> ret = queryBuilder.orderRaw(mOrderBy).limit(CURSOR_LIMIT).list();
        return ret;
    }

    @Override
    public int getGroupCount() {
        return mCursor.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return mCursor.get(i);
    }

    @Override
    public Object getChild(int i, int i2) {
        return mCursor.get(i);
    }

    @Override
    public long getGroupId(int i) {
        return mCursor.get(i).getId();
    }

    @Override
    public long getChildId(int i, int i2) {
        return mCursor.get(i).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_simple, null);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.text_summary);
        textView.setText(mCursor.get(groupPosition).getHoTen());
        //get the string item from the position "position" from array list to put it on the TextView
        if (mCursor.size()==1) {
            try {
                ((ExpandableListView)parent).expandGroup(0);
            }
            catch (Exception e) {
                Log.e(TAG, "Error", e);
            }
        }
        return convertView;


    }

    @Override
    public View getChildView(int i, int i2, boolean b, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.detail_patient, null);
        }

        final BenhNhan benhNhan = ((BenhNhan)getGroup(i));

        WhereCondition condition = DaKhamDao.Properties.MaSoBenhNhan.eq(benhNhan.getMaSo());
        List<DaKham> daKhamList = mDaKhamDao.queryBuilder().where(condition).list();

        for (DaKham daKham: daKhamList) {
            ((CheckBox)convertView.findViewWithTag(daKham.getMucKham())).setChecked(true);
            convertView.findViewWithTag(daKham.getMucKham()).setBackgroundColor(
                    convertView.getResources().getColor(R.color.green));
        }

        Button buttonKham = ((Button)convertView.findViewById(R.id.button_kham));
        buttonKham.setText(
                String.format("Khám bệnh %s", benhNhan.getHoTen())
        );
        buttonKham.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CustomApplication)mContext.getApplicationContext()).benhNhan = benhNhan;
                mContext.startActivity(
                        new Intent(mContext, DiagnosticActivity.class)
                );
            }
        });


        ((TextView)convertView.findViewById(R.id.gioi_tinh)).setText(
                String.format("Giới tính: %s", sGioiTinhMap.get(benhNhan.getGioiTinh()))
        );
        ((TextView)convertView.findViewById(R.id.nam_sinh)).setText(
                String.format("Năm sinh: %s", StringUtils.nullToEmpty(benhNhan.getNgaySinh()))
        );
        ((TextView)convertView.findViewById(R.id.ma_so)).setText(
                String.format("Mã số: %s", benhNhan.getMaSo())
        );
        ((TextView)convertView.findViewById(R.id.ma_nv)).setText(
                String.format("Mã NV: %s", StringUtils.nullToEmpty(benhNhan.getMaNhanVien()))
        );
        ((TextView)convertView.findViewById(R.id.bo_phan)).setText(
                String.format("Bộ phận: %s", "")
        );

        List<KetQua> ketQuaList = mKetQuaDao.queryBuilder().where(
                KetQuaDao.Properties.MaSoBenhNhan.eq(benhNhan.getMaSo())
                , KetQuaDao.Properties.Huy.notEq(true)
        ).list();

        LinearLayout linear = (LinearLayout)convertView;
        final int childCount = linear.getChildCount();
        ArrayList<TextView> reusedViews = new ArrayList<TextView>();
        for (int childIndex=0; childIndex<childCount; childIndex++) {
            if (linear.getChildAt(childIndex).getTag()=="ket_qua") {
                reusedViews.add((TextView)linear.getChildAt(childIndex));
            }
        }
        int toAdd = ketQuaList.size() - reusedViews.size();
        for (int childIndex=0; childIndex<toAdd; childIndex++){
            TextView textView = new TextView(convertView.getContext());
            textView.setTag("ket_qua");
            linear.addView(textView);
            reusedViews.add(textView);
        }
        int toRemove = reusedViews.size() - ketQuaList.size();
        for (int childIndex=0;childIndex<toRemove ; childIndex++){
            linear.removeView(reusedViews.get(childIndex));
            ketQuaList.remove(childIndex);
        }
        for (int childIndex=0; childIndex<ketQuaList.size(); childIndex++){
            KetQua ketQua = ketQuaList.get(childIndex);
            String ketQuaString = String.format("%s : %s. Phân loại %s",
                    ModalityCollection.getModality(ketQua.getMucKham()).modalityLabel,
                    ketQua.getChanDoan(),
                    ketQua.getPhanLoai()
            );
            reusedViews.get(childIndex).setText(ketQuaString);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return false;
    }

    /**
     * Static class used to avoid the calling of "findViewById" every time the getView() method is called,
     * because this can impact to your application performance when your list is too big. The class is static so it
     * cache all the things inside once it's created.
     */
    private static class ViewHolder {

        protected TextView itemName;

    }
}