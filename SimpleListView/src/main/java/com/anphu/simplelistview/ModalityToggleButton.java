package com.anphu.simplelistview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ToggleButton;

/**
 * Created by nguyenivan on 12/2/13.
 */

public class ModalityToggleButton extends ToggleButton {
    public String modalityId;

    public ModalityToggleButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ModalityToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ModalityToggleButton(Context context) {
        super(context);
    }
}
