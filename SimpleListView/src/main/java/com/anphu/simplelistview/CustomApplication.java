package com.anphu.simplelistview;

import android.app.Application;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.anphu.simplelistview.db.BenhNhan;
import com.anphu.simplelistview.db.BenhNhanDao;
import com.anphu.simplelistview.db.DaKhamDao;
import com.anphu.simplelistview.db.DaoMaster;
import com.anphu.simplelistview.db.DaoSession;
import com.anphu.simplelistview.db.KetQuaDao;
import com.anphu.simplelistview.db.KetQuaMauDao;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by nguyenivan on 11/30/13.
 */
public class CustomApplication extends Application {
    public static final String TAG = "CustomAplication";

    public static String BASE_URL = "";
    public static String UPDATE_URL = BASE_URL + "sync/";
    public static String GOIKHAM_URL = BASE_URL + "sync/goikham/";
    public static String DAKHAM_URL = BASE_URL + "sync/dakham/";
    public static String KETQUA_URL = BASE_URL + "sync/ketqua/";
    public static String DEVICE_ID = "";

    public String bacSy;
    public String goiKham;
    public String maGoiKham;
    public BenhNhan benhNhan;
    public DaoMaster daoMaster;
    public DaoSession daoSession;
    public BenhNhanDao benhNhanDao;
    public DaKhamDao daKhamDao;
    public KetQuaMauDao ketQuaMauDao;
    public KetQuaDao ketQuaDao;
    public SQLiteDatabase db;



    private SharedPreferences.OnSharedPreferenceChangeListener mListener;

    public CustomApplication() {
        super();
    }

    private void setUrls(String hostName) {
        BASE_URL = String.format("http://%s/",hostName.replace("/",""));
        UPDATE_URL = BASE_URL + "sync/";
        GOIKHAM_URL = BASE_URL + "sync/goikham/";
        DAKHAM_URL = BASE_URL + "sync/dakham/";
        KETQUA_URL = BASE_URL + "sync/ketqua/";
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        db = new DaoMaster.DevOpenHelper(this, "benhnhan-db", null).getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        benhNhanDao = daoSession.getBenhNhanDao();
        daKhamDao = daoSession.getDaKhamDao();
        ketQuaDao = daoSession.getKetQuaDao();
        ketQuaMauDao = daoSession.getKetQuaMauDao();

        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            mListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    if (key==getResources().getString(R.string.key_sync_host_name)){
                        String hostName = prefs.getString(key, null);
                        if (hostName!=null) {
                            setUrls(hostName);
                        }
                        return;
                    }
                    if (key.equals(getResources().getString(R.string.key_bac_sy))){
                        bacSy = prefs.getString(key,"");
                    }
                    if (key.equals(getResources().getString(R.string.key_goi_kham))){
                        goiKham = prefs.getString(key,"");
                    }
                    if (key.equals(getResources().getString(R.string.key_ma_goi_kham))){
                        maGoiKham = prefs.getString(key,"");
                    }



                }
            };
            prefs.registerOnSharedPreferenceChangeListener(mListener);

            DEVICE_ID = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            String hostName = prefs.getString(
                    getResources().getString(R.string.key_sync_host_name) ,
                    getResources().getString(R.string.default_sync_host_name)
            );
            if (hostName!=null) {
                setUrls(hostName);
            }
            bacSy = prefs.getString(getResources().getString(R.string.key_bac_sy),"");
            goiKham = prefs.getString(getResources().getString(R.string.key_goi_kham),"");
            maGoiKham = prefs.getString(getResources().getString(R.string.key_ma_goi_kham),"");

        }
        catch (Exception e) {
            Log.e(TAG, "Error getting preferences", e);
        }
        // Initialize the singletons so their instances
        // are bound to the application process.
        initSingletons();
    }

    protected void initSingletons()
    {
        // Initialize the instance of MySingleton
        ModalityCollection.initInstance(getApplicationContext());
    }
}
