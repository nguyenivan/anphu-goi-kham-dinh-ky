package com.anphu.simplelistview;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.anphu.simplelistview.db.BenhNhan;
import com.anphu.simplelistview.db.BenhNhanDao;
import com.anphu.simplelistview.sync.SyncUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by nguyenivan on 12/2/13.
 */
public class PatientFragment extends Fragment{

    private static final String TAG = "PatientFragment";
    private PatientListAdapter mAdapter;

    public PatientFragment(){
        super();
    }

//    private  SearchView searchView;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mAdapter = new PatientListAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_patient, container, false);

        final EditText editText=(EditText)view.findViewById(R.id.edit_search);
        editText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
                mAdapter.setQuery(s.toString());
            }
        });

        final Button switchKeyboard = (Button)view.findViewById(R.id.button_switch);
        switchKeyboard.setTag(0);
        switchKeyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int status = (Integer) view.getTag();
                switch (status) {
                    case 0:
                        switchKeyboard.setText(R.string.numeric_keyboard);
                        editText.setInputType(InputType.TYPE_CLASS_TEXT);
                        view.setTag(1);
                        break;
                    case 1:
                        switchKeyboard.setText(R.string.text_keyboard);
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        view.setTag(0);
                        break;
                }
                editText.requestFocus();
            }
        });
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);

        ExpandableListView listView = (ExpandableListView) view.findViewById(R.id.list_patient);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(), "Chọn Bệnh Nhân " + Integer.toString(i)  , Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private BenhNhan addBenhNhan(String s) {
        Toast.makeText(getActivity(), "Add Benh Nhan " + s, Toast.LENGTH_SHORT).show();
        BenhNhan benhNhan = new BenhNhan();
        benhNhan.setMaSo(String.format("AA%d04", new Random().nextInt(9999) ));
        benhNhan.setHoTen(s);
        benhNhan.setHonNhan(s);
        benhNhan.setGioiTinh("nam");
        benhNhan.setNgaySinh(Calendar.getInstance().getTime());
        benhNhan.setGoiKham("goi_kham");
        ((CustomApplication)getActivity().getApplication()).benhNhanDao.insert(benhNhan);
        return benhNhan;
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//        SyncUtils.CreateSyncAccount(activity);
//    }
}