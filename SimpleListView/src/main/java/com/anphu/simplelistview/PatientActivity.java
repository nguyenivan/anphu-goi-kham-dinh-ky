package com.anphu.simplelistview;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by nguyenivan on 12/2/13.
 */
public class PatientActivity extends ActionBarActivity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new PatientFragment()).commit();
    }
}